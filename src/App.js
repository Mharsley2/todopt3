import React, { Component } from "react";
import "./index.css";
import todosList from "./todos.json";
import TodoList from "./TodoList.jsx";
import { BrowserRouter as Router, Route, NavLink } from "react-router-dom";

class App extends Component {
  state = {
    todos: todosList,
    input: ""
  };
  //toggle todo complete
  handleToggleClick = todo => event => {
    let tempState = this.state;
    todo.completed = !todo.completed;
    this.setState({ tempState });
  };
  //
  handleDestroyClick = todo => event => {
    let tempState = this.state;
    let newTodos = tempState.todos.filter(item => item.id !== todo.id);
    this.setState({ todos: newTodos });
  };

  //create functionality clear completeclick
  handleClearCompleteClick = event => {
    let tempState = this.state;
    let tempTodos = this.state.todos.filter(todo => todo.completed === false);
    tempState.todos = tempTodos;
    this.setState({ tempState });
  };
  //create todo(handle it)
  handleCreateTodo = event => {
    if (event.keyCode === 13) {
      let tempState = this.state;

      tempState.todos.push({
        userId: 1,
        id: Math.ceil(Math.random() * 100000),
        title: tempState.input,
        completed: false
      });
      //state of todo
      this.setState({ todos: tempState.todos, input: "" });
      event.target.value = "";
    }
  };
  //handle the change!
  handleChange = event => {
    this.setState({ input: event.target.value });
  };
  render() {
    return (
      <Router>
        <section className="todoapp">
          <header className="header">
            <h1>todos</h1>
            <input
              className="new-todo"
              placeholder="What needs to be done?"
              autoFocus
              //was confused when terminal was like "error, autofocus what?" well this is why!
              value={this.state.input}
              onChange={this.handleChange}
              onKeyDown={this.handleCreateTodo}
            />
          </header>
          {/* lets get this routing done */}
          <Route
            exact
            path="/"
            render={() => (
              <TodoList
                todos={this.state.todos}
                handleToggleClick={this.handleToggleClick}
                handleDestroyClick={this.handleDestroyClick}
              />
            )}
          />
          <Route
            path="/active"
            render={() => (
              <TodoList
                todos={this.state.todos.filter(todo => {
                  if (todo.completed === false) {
                    return todo;
                  }
                  return false;
                })}
                handleToggleClick={this.handleToggleClick}
                handleDestroyClick={this.handleDestroyClick}
              />
            )}
          />
          <Route
            path="/completed"
            render={() => (
              <TodoList
                todos={this.state.todos.filter(todo => {
                  if (todo.completed === true) {
                    return todo;
                  }
                  return false;
                })}
                handleToggleClick={this.handleToggleClick}
                handleDestroyClick={this.handleDestroyClick}
              />
            )}
          />
          <footer className="footer">
            {/* <!-- This should be `0 items left` by default --> */}
            <span className="todo-count">
              <strong>
                {
                  this.state.todos.filter(todo => {
                    if (todo.completed === false) {
                      return todo;
                    }
                    return false;
                  }).length
                }
              </strong>{" "}
              item(s) left
            </span>
            <ul className="filters">
              <li>
                <NavLink exact to="/" activeClassName="selected">
                  All
                </NavLink>
              </li>
              <li>
                <NavLink to="/active" activeClassName="selected">
                  Active
                </NavLink>
              </li>
              <li>
                <NavLink to="/completed" activeClassName="selected">
                  Completed
                </NavLink>
              </li>
            </ul>
            {/* made sure that clear button still works! */}
            <button
              className="clear-completed"
              onClick={this.handleClearCompleteClick}
            >
              Clear completed
            </button>
          </footer>
        </section>
      </Router>
    );
  }
}

// put router tag at highest level to intercept any history changes and add new path
// for new page into browser history stack

export default App;
